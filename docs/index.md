# Setux

Python framework to install / deploy / maintain local or remote hosts.

[PyPI] - [Repo] - [Doc]

## Abstract

This is a meta package including all the packages needed for setux to be functionnal.

Installing this package will install the following packages as dependencies.

### [setux core]
`setux.core`
Base classes for all other packages.

### [setux distros]
`setux.core.distro.Distro` implementations
Supported OSs (Debian, FreeBSD)

### [setux targets]
`setux.core.target.Target` implementations
Connection to the target machine (Local, SSH)

### [setux managers]
`setux.core.manage.Manager` implementations
Resources managers (Packages, Services)

### [setux mappings]
`setux.core.mapping.Mapping` implementations
Mapping resources names (Packages, Service)

### [setux modules]
`setux.core.module.Module` implementations
User defined functionality

### [setux actions]
`setux.core.action.Action` implementations
Top level User defined functionality

### [setux logger]
`setux.core.logger.Logger` implementation
Default logger


## Usage

### Python

```python
import setux
```

## Install

```bash
pip install setux
```

Note : `Additional Setux packages` install Setux as a dependency.

### System requirements

- Python 3.11+
- Pip
- ssh
- rsync

# Additional packages

## [setux REPL]

Rudimentary Setux REPL / CLI

note : Setux is mainly intended to be used as a Python framework.

## [setux PLUS]

Augmented Setux distribution

note : Additional implementations of `setux.core` base classes.


[PyPI]: https://pypi.org/project/setux
[Repo]: https://framagit.org/louis-riviere-xyz/setux
[Doc]: https://setux.readthedocs.io
[setux core]: https://setux-core.readthedocs.io
[setux logger]: https://setux-logger.readthedocs.io
[setux modules]: https://setux-modules.readthedocs.io
[setux actions]: https://setux-actions.readthedocs.io
[setux mappings]: https://setux-mappings.readthedocs.io
[setux managers]: https://setux-managers.readthedocs.io
[setux targets]: https://setux-targets.readthedocs.io
[setux distros]: https://setux-distros.readthedocs.io
[setux REPL]: https://setux-repl.readthedocs.io
[setux PLUS]: https://setux-plus.readthedocs.io
