# Setux

Python framework to install / deploy / maintain local or remote hosts.

[PyPI] - [Repo] - [Doc]


[PyPI]: https://pypi.org/project/setux
[Repo]: https://framagit.org/louis-riviere-xyz/setux
[Doc]: https://setux.readthedocs.io
